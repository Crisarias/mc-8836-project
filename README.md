# mc-8836-project

Repository for source code of Paper Performance comparison of Chapel vs MPI andOpenMP using the N-body problem on HPC

## Installations of environments

### VSCODE

### GCC / GCC++ / OpenMP

https://www.fosslinux.com/39386/how-to-install-multiple-versions-of-gcc-and-g-on-ubuntu-20-04.htm

* Change environments:

`
    sudo update-alternatives --config gcc
    sudo update-alternatives --config g++
`

### MPI

Not needed results already

### Chapel

Follow 

https://chapel-lang.org/docs/usingchapel/prereqs.html#readme-prereqs
https://chapel-lang.org/docs/usingchapel/QUICKSTART.html

1. Install prerequisites:
    `
        sudo apt-get install m4 perl python3 python3-pip python3-venv python3-dev bash make mawk git pkg-config
    `
2. wget https://github.com/chapel-lang/chapel/releases/download/1.24.1/chapel-1.24.1.tar.gz
3. tar xzf chapel-1.24.1.tar.gz
4. cd chapel-1.24.1
5. source util/setchplenv.bash
6. make
7. make check

