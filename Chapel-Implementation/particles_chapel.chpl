/**
 * Costa Rica Institute of Technology
 * School of Computing
 * Parallel Computing (MC-8836)
 * Instructor Esteban Meneses, PhD (esteban.meneses@acm.org)
 * Chapel particle-interaction code.
 */
config const particlesFile: string, numParticles: int;

// Particle-interaction constants
const A: real = 10250000.0;
const B: real = 726515000.5;

use IO;
use Time;

type Particle = 5*real;
type Forces = 2*atomic real;

// Function for printing out the particle array
proc print_particles(ref particles) {
  writeln("Index\tx\ty\tmass\tfx\tfy");
  for i  in 1..numParticles {
	  var p = particles[i];
	  writeln(p(0), "\t", p(1), "\t", p(2), "\t", p(3), "\t", p(4));
  }
}

// Reads particle information from a text file
proc readFile() {
  var infile = open(particlesFile, iomode.r);
  var reader = infile.reader();  
  var particles: [1..numParticles] Particle;
  var line:string;
  for i  in 1..numParticles {
	  reader.readline(line);
	  var values = line.split(-1);
	  particles[i] = (values(0):real, values(1):real, values(2):real, 0, 0);
  }
  reader.close();
  infile.close();
  return particles;
}

// Function for computing interaction between two sets of particles
proc compute_self_interaction(ref particles) {
	var forces: [1..numParticles] Forces;
	// Calculate forces
	forall i in 1..numParticles { 
		var from = i + 1;
		for j in from..numParticles {
				var rx,ry,r,fx,fy,f: real;
				rx = particles[i](0) - particles[j](0);
				ry = particles[i](1) - particles[j](1);
				r = sqrt(rx*rx + ry*ry);
				if(r != 0.0) {
					f = A / r ** 6 - B / r ** 12;
					fx = f * rx / r;
					fy = f * ry / r;			
					forces[i](0).add(fx);
					forces[i](1).add(fy);
					forces[j](0).sub(fx);
					forces[j](1).sub(fy);
				}
		}
	}
	// Update forces
	forall i in 1..numParticles { 
		particles[i](3) = forces[i](0).read();
		particles[i](4) = forces[i](1).read();
	}
}

proc main() {
	var t: Timer;
	t.start();
	var particles = readFile();	
	compute_self_interaction(particles);
	t.stop();
	writeln("Duration ", t.elapsed(), " seconds");
	// print_particles(particles);
	return 0;
}