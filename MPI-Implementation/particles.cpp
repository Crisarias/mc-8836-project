/**
 * Costa Rica Institute of Technology
 * School of Computing
 * Parallel Computing (MC-8836)
 * Instructor Esteban Meneses, PhD (esteban.meneses@acm.org)
 * MPI particle-interaction code. 
 */

#include "mpi.h"
#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>

using namespace std;

#define TAG 7

// Particle-interaction constants
#define A 10250000.0
#define B 726515000.5

// Structure for shared properties of a particle (to be included in messages)
struct Particle{
	float x;
	float y;
	float mass;
	float fx;
	float fy;
};

// Headers for auxiliar functions
void print_particles(struct Particle *particles, int n);
void interact(struct Particle *source, struct Particle *destination);
void compute_interaction(struct Particle *source, struct Particle *destination, int limit);
void compute_self_interaction(struct Particle *set, int size);
void merge(struct Particle *first, struct Particle *second, int limit);
int read_file(struct Particle *set, int size, char *file_name);

// Main function
main(int argc, char** argv){
	int myRank;									// Rank of process
	int p;										// Number of processes
	int n;										// Number of total particles
	int previous;								// Previous rank in the ring
	int next;									// Next rank in the ring
	int tag = TAG;								// Tag for message
	int number;									// Number of local particles
	struct Particle *globals;					// Array of all particles in the system
	struct Particle *locals;					// Array of local particles
	struct Particle *remotes;					// Array of foreign particles
	char *file_name;							// File name
	MPI_Status status;							// Return status for receive
	int j, rounds, initiator, sender;
	double start_time, end_time;

	// checking the number of parameters
	if(argc < 3){
		printf("ERROR: Not enough parameters\n");
		printf("Usage: %s <number of particles> [<file>]\n", argv[0]);
		exit(1);
	}
	
	// getting number of particles
	n = atoi(argv[1]);

	// initializing MPI structures and checking p is odd
	MPI_Init(&argc, &argv);
	MPI_Comm_rank(MPI_COMM_WORLD, &myRank);
	MPI_Comm_size(MPI_COMM_WORLD, &p);
	if(p % 2 == 0){
		p = p - 1;
		if(myRank == p){
			MPI_Finalize();
			return 0;
		}
	}

	// acquiring memory for particle arrays
	number =  n / p;	
	// If exists a remaining and cannot be distribute equally modify locals and remotes
	
	int remaining = n % p;
	if (remaining != 0) {
		number =  (n / p) + 1;
	}	
	locals = (struct Particle *) malloc(number * sizeof(struct Particle));
	remotes = (struct Particle *) malloc(number * sizeof(struct Particle));

	// define a particle as 5 floats
	MPI_Datatype PARTICLE;
	MPI_Type_contiguous( 5, MPI_FLOAT, &PARTICLE );
	MPI_Type_commit( &PARTICLE );

	// starting timer
	if(myRank == 0){
		start_time = MPI_Wtime();
	}

	// checking for file information
	if(myRank == 0) {
		globals = (struct Particle *) malloc(number * p * sizeof(struct Particle));

		// YOUR CODE GOES HERE (reading particles from file)
		int result = read_file(globals, n, argv[2]);
	}
		
	// YOUR CODE GOES HERE (distributing particles among processors)
	// Scatter the particles to all processes
	MPI_Scatter(globals, number, PARTICLE, locals, number, PARTICLE, 0, MPI_COMM_WORLD);

	rounds= (p-1) / 2;
	previous = (myRank + p - 1) % p;
	next = (myRank + 1) % p;
	initiator = (myRank + p + rounds + 1) % p;
	sender = (myRank + rounds) % p;
	MPI_Send(locals, number, PARTICLE, next, tag, MPI_COMM_WORLD);
	// YOUR CODE GOES HERE (ring algorithm)	
	for(int i=0; i <= rounds; i++) {	
		bool isLastRoundPassed = i == rounds;	
		if (!isLastRoundPassed) {
			MPI_Recv(remotes, number, PARTICLE, previous, tag, MPI_COMM_WORLD, &status);
			compute_interaction(locals, remotes, number);
			MPI_Send(remotes, number, PARTICLE, next, tag, MPI_COMM_WORLD);
		} else {
			MPI_Send(remotes, number, PARTICLE, initiator, tag, MPI_COMM_WORLD);
		}		
	}
	MPI_Recv(remotes, number, PARTICLE, sender, tag, MPI_COMM_WORLD, &status);		
	
	//Calculate
	merge(locals, remotes, number);
	compute_self_interaction(locals,number);
	
	// printing information on particles
	MPI_Gather(locals, number, PARTICLE, globals, number, PARTICLE, 0, MPI_COMM_WORLD);

	// stopping timer
	if(myRank == 0){
		end_time = MPI_Wtime();
		printf("Duration: %f seconds\n", (end_time-start_time));
	}

	// if(myRank == 0) {
		// print_particles(globals,n);
	// }


	// finalizing MPI structures
	MPI_Finalize();
}

// Function for printing out the particle array
void print_particles(struct Particle *particles, int n){
	int j;
	printf("Index\tx\ty\tmass\tfx\tfy\n");
	for(j = 0; j < n; j++){
		printf("%d\t%f\t%f\t%f\t%f\t%f\n",j,particles[j].x,particles[j].y,particles[j].mass,particles[j].fx,particles[j].fy);
	}
}

// Function for computing interaction among two particles
// There is an extra test for interaction of identical particles, in which case there is no effect over the destination
void interact(struct Particle *first, struct Particle *second){
	float rx,ry,r,fx,fy,f;

	// computing base values
	rx = first->x - second->x;
	ry = first->y - second->y;
	r = sqrt(rx*rx + ry*ry);

	if(r == 0.0)
		return;

	f = A / pow(r,6) - B / pow(r,12);
	fx = f * rx / r;
	fy = f * ry / r;

	// updating sources's structure
	first->fx = first->fx + fx;
	first->fy = first->fy + fy;
	
	// updating destination's structure
	second->fx = second->fx - fx;
	second->fy = second->fy - fy;

}

// Function for computing interaction between two sets of particles
void compute_interaction(struct Particle *first, struct Particle *second, int limit){
	int j,k;
	
	for(j = 0; j < limit; j++){
		for(k = 0; k < limit; k++){
			interact(&first[j],&second[k]);	
		}
	}
}

// Function for computing interaction between two sets of particles
void compute_self_interaction(struct Particle *set, int size){
	int j,k;
	
	for(j = 0; j < size; j++){
		for(k = j+1; k < size; k++){
			interact(&set[j],&set[k]);	
		}
	}
}

// Function to merge two particle arrays
// Permanent changes reside only in first array
void merge(struct Particle *first, struct Particle *second, int limit){
	int j;
	
	for(j = 0; j < limit; j++){
		first[j].fx += second[j].fx;
		first[j].fy += second[j].fy;
	}
}

// Reads particle information from a text file
int read_file(struct Particle *set, int size, char *file_name){
	ifstream file(file_name);
	if(file.is_open()){
		
		// reading particle values
		for(int i=0; i<size; i++){
			file >> set[i].x;
			file >> set[i].y;
			file >> set[i].mass;
			set[i].fx = 0.0;
			set[i].fy = 0.0;
		}

		// closing file
		file.close();

	} else {
		cout << "Error opening file: " << file_name << endl;
		return 1;
	}
	return 0;
}


