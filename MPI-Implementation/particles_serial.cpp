/**
 * Costa Rica Institute of Technology
 * School of Computing
 * Parallel Computing (MC-8836)
 * Instructor Esteban Meneses, PhD (esteban.meneses@acm.org)
 * Serial particle-interaction code. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <iostream>
#include <fstream>
#include "timer.h"

using namespace std;

// Particle-interaction constants
#define A 10250000.0
#define B 726515000.5

// Structure for shared properties of a particle (to be included in messages)
struct Particle{
	float x;
	float y;
	float mass;
	float fx;
	float fy;
};

// Headers for auxiliar functions
void print_particles(struct Particle *set, int n);
void interact(struct Particle *source, struct Particle *destination);
void compute_self_interaction(struct Particle *set, int size);
int read_file(struct Particle *set, int size, char *file_name);

// Main function
main(int argc, char** argv){
	int n;										// Number of total particles
	struct Particle *locals;					// Array of local particles
	char *file_name;							// File name}

	// starting timer
	timerStart();	
	
	// getting number of particles
	n = atoi(argv[1]);

	// acquiring memory for particle arrays
	locals = (struct Particle *) malloc(n * sizeof(struct Particle));

	read_file(locals,n,argv[2]);

	// particle interaction
	compute_self_interaction(locals,n);
	
	// stopping timer
	double duration = timerStop();

	printf("Duration: %f seconds\n", duration);
	
	// print_particles(locals,n);
	
}

// Function for printing out the particle array
void print_particles(struct Particle *particles, int n){
	int j;
	printf("Index\tx\ty\tmass\tfx\tfy\n");
	for(j = 0; j < n; j++){
		printf("%d\t%f\t%f\t%f\t%f\t%f\n",j,particles[j].x,particles[j].y,particles[j].mass,particles[j].fx,particles[j].fy);
	}
}

// Function for computing interaction among two particles
// There is an extra test for interaction of identical particles, in which case there is no effect over the destination
void interact(struct Particle *first, struct Particle *second){
	float rx,ry,r,fx,fy,f;

	// computing base values
	rx = first->x - second->x;
	ry = first->y - second->y;
	r = sqrt(rx*rx + ry*ry);

	if(r == 0.0)
		return;

	f = A / pow(r,6) - B / pow(r,12);
	fx = f * rx / r;
	fy = f * ry / r;

	// updating sources's structure
	first->fx = first->fx + fx;
	first->fy = first->fy + fy;
	
	// updating destination's structure
	second->fx = second->fx - fx;
	second->fy = second->fy - fy;
}

// Function for computing interaction between two sets of particles
void compute_self_interaction(struct Particle *set, int size){
	int j,k;
	
	for(j = 0; j < size; j++){
		for(k = j+1; k < size; k++){
			interact(&set[j],&set[k]);	
		}
	}
}

// Reads particle information from a text file
int read_file(struct Particle *set, int size, char *file_name){
	ifstream file(file_name);
	if(file.is_open()){
		
		// reading particle values
		for(int i=0; i<size; i++){
			file >> set[i].x;
			file >> set[i].y;
			file >> set[i].mass;
			set[i].fx = 0.0;
			set[i].fy = 0.0;
		}

		// closing file
		file.close();

	} else {
		return 1;
	}
	return 0;
}
