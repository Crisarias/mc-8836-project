# OPEN MP Implementation

## To run on local develoopment

* Download an configure docker
* pull the image mfisherman/mpich
* execute the following command:

`
 docker run --rm -it -w /app -v ${PWD}:/app mfisherman/mpich
`

* make
* To test use the following command:

`
OMP_NUM_THREADS=7 ./particles_openmp 31 test_particles_2.txt
`

`
./particles_serial 31 test_particles_2.txt
`